<?php
use yii\helpers\Html;
$this->title = 'Noticias';
?>


<div class="container">
    <div class="row stilobody">
        <div class="align-items-center col-10" style="padding: 30px">
            <p>VUELTA CICLISTA A ESPAÑA</p>
            <h1>Gorospe cedio el liderato al italiano Giovannetti</h1>
            <hr>
            <p class="text-justify">Un aspirante al mero triunfo de etapa, Carlos Hernández (Lotus), volvió a demostrar ayer que los favoritos de la Vuelta continúan todavía lejos de imponer su autoridad sobre la carrera. Hernández se adjudicó la victoria en una jornada, la 11ª entre León y la Estación de San Isidro, de 203 kilómetros, en la que los grandes se limitaron a rodar fuerte. Sólo los que no lograron mantener el ritmo cedieron tiempo, entre ellos el líder, Julián Gorospe (Banesto), que cedió su jersei amarillo al italiano Marco Giovannetti (Seur), un buen corredor que fue sexto en un Giro, pero cuyas perspectivas para mantenerlo son escasas.</p>
            <p class="text-justify">La jerarquía de la Vuelta sigue perfilándose de un modo imperceptible. La carrera hasta ahora se ha desarrollado al mejor estilo de las pruebas por eliminación. Los que la dominan no han conseguido su posición por haber desencadenado ataques individuales, sino por no haber perdido rueda respecto al grupo selecto de favoritos. Ello a pesar de que se ha superado ya el meridiano de la prueba y de que ayer se disputó la segunda de las cinco etapas con final en montaña.Los eliminados ayer fueron corredores con posibilidades, pero que no figuran entre los máximos favoritos. El hecho de que Gorospe cediera su liderato y de que Etxabe, Unzaga, Ivanov, Herrera y Lejarreta perdieran tiempo no supuso ninguna gran sorpresa.</p>
            <p class="text-justify">El respeto entre los favoritos y tal vez el no disimulado miedo a pagar el sobreesfuerzo evitó que apenas se produjera atque alguno en el furgón de élite. Únicamente Gastón, a dos kilómetros, se atrevió a adelantarse ligeramente y sin éxito. Los últimos 14 kilómetros y las rampas del puerto de San Isidro tampoco eran excesivamente duros, no lo suficiente para invitar a que se rompiese el grupo.</p>
            <br>
            <h4>Tramo final</h4>
            <p class="text-justify">Las acciones esenciales en el tramo Final se basaron en las apetencias del equipo Seur en conseguir el liderato. El primer síntoma de debilidad de Gorospe, a unos 14 kilómetros de la llegada, fue aprovechado por Pino para marcar un ritmo más exigente. Entre él y el colombiano Farfán (Kelme) provocaron la lenta desaparición en el grupo de Gorospe, del colombiano Herrera -ganador de la Vuelta en 1987-, del soviético Ivanov -sexto en la edición del año pasado y cuarto hasta ayer- y de Etxabe, que también intentó un ,breve ataque pero que acabó cediendo terreno.</p>
            <p class="text-justify">Unzaga y Lejarreta, que fueron los máximos impulsores de la escapada que determinó el curso de la etapa en sus 170 primeros kilómetros, también cedieron tiempo. El primero resistió casi hasta el final las acometidas de los favoritos. Lejarreta se mantuvo en cabeza hasta que sufrió una caída, que acabó por hacerle imposible mantener el ritmo de Montoya, que le dejó en el descenso de la Cobertoria, puerto de primera categoría, a 60 kilómetros de la meta.</p>
            <p class="text-justify">Montoya fue la baza que utilizó el BH para intentar su segundo triunfo de etapa, pero acabó sufriendo un desfallecimiento espectacular cuando faltaban sólo seis kilómetros. Carlos Hernández, bicampeón de España, le rebasó con una soltura espectacular que le permitió llegar en solitario y adjudicarse su tercera etapa de la Vuelta a España a lo largo de su carrera.</p>
            <p class="text-justify">La victoria de Hernández es otro síntoma de las dificultades de los favoritos para imponer su hegemonía. Los que luchan únicamente por la victoria de etapa les superan con cierta facilidad, como ya sucedió en Sierra Nevada, donde se impuso el francés Esnault (BH).</p>
            <p class="text-justify">Otro detalle a tener en cuenta en este momento de la Vuelta es que los colombianos no acaban de obtener fruto de la calidad de sus numerosos escaladores, y cada vez seven más resignados a esperar lo que pueda hacer Parra y tal vez Cadena, que sin haber realizado nada espectacular ha mantenido una regularidad que le permite estar segundo en la general.</p>
        </div>
        <div class="col-2 card shadow-sm" style="background-color: buttonhighlight">
            <h3 style="text-align: center">Noticias</h3>
            <div class="row">
                <div class="col card shadow-sm">
                    <h4 class="text-justify">Los directores de equipos asumen el riesgo de sus tácticas</h4>
                    <p class="text-justify">Los directores deportivos de los equipos españoles asumen el notable riesgo del peligroso juego táctico que han utilizado en las seis primeras etapas de la Vuelta 
                       <?= Html::a('Seguir leyendo...',['site/noticia','num'=>'2'])?>
                    </p>
                </div>
            </div>
             <div class="row">
                <div class="col card shadow-sm">
                    <h4 class="text-justify">Primer golpe de mano de Banesto en la Vuelta</h4>
                    <p class="text-justify">La omnipresencia parece ser la regla de (oro para todo aspirante a ganar la actual edición de la Vuelta a España. Desaparecer de la cabeza del pelotón, tan siquiera fugazmente, conduce directamente al furgón de cola
                       <?= Html::a('Seguir leyendo...',['site/noticia','num'=>'3'])?>
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>