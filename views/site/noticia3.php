<?php
use yii\helpers\Html;
$this->title = 'Noticias';
?>


<div class="container">
    <div class="row stilobody">
        <div class="align-items-center col-10" style="padding: 30px">
            <p>VUELTA CICLISTA A ESPAÑA</p>
            <h1>Primer golpe de mano de Banesto en la Vuelta</h1>
            <hr>
            <p class="text-justify">La omnipresencia parece ser la regla de (oro para todo aspirante a ganar la actual edición de la Vuelta a España. Desaparecer de la cabeza del pelotón, tan siquiera fugazmente, conduce directamente al furgón de cola. Ayer lo comprobaron Lejarreta y Cabestany, Pino, y el suizo Rominger, que perdieron 52 segundos respecto al grupo de los mejores, que voló bajo la impulsión del equipo Banesto de Delgado e Induráin. El vencedor de la cuarta etapa (Murcia-Almería, 233 kilómetros) fue el holandés Erwin Nijboer (Stuttgart), que junto al italiano Tebaldi (Chateau D'Ax) se escapó 200 kilómetros.</p>
            <p class="text-justify">Los corredores del Banesto parecían necesitados de un golpe de mano como el que protagonizaron ayer para recuperar el tiempo y la moral tras las dos primeras jornadas en las que no obtuvo los resultados que cabían esperar de su potencial. Todo hacía suponer, incluso el desarrollo de los 170 primeros kilómetros, que la etapa iba a ser de transición ante la primera llegada en alta montaña de hoy. Pero la determinación para aprovechar el menor resquicio quedó demostrada cuando se entraba ya en el tramo final y la lucha parecía reducida a los buscadores de triunfos de etapa.El equipo Banesto, que se adueñó de las primeras posiciones del pelotón, entre las aceleraciones de algunos corredores poco importantes para la general como Sánchez (Teka) y Sanchis (Seur) y el viento que empezó a soplar de costado, logró formar un primer abanico y romper la carrera. Más de la mitad del equipo Banesto estaba en ese grupo: Induráin, Delgado, Gorospe, Alonso y Oliver. Atrás quedaban otros hombres importantes. La oportunidad de distanciarlos fue aprovechada por los corredores de José Miguel Echávarri.</p>
            <p class="text-justify">La etapa dejó en evidencia a los que no pueden mantener la tensión constante que significa estar en todo momento en la cabeza del pelotón. El hundimiento fue especialmente espectacular en el caso del francés Jean François Bernard, tercero en el Tour en 1987, que cedió 1.36 minutos respecto a Delgado, Induráin y compañía. Los estragos fueron igualmente importantes para la ONCE, el Seúr y el Teka, y no perjudicaron a algunos de los corredores que temen especialmente este tipo de situaciones, como el colombiano Herrera o incluso Parra, y como el soviético Klimov, que mantuvo con brillantez su liderato.</p>
            <p class="text-justify">El resto de la jornada se caracterizó por la larga escapada que inició el italiano Tebaldi en el kilómetro 17 y a la que se sumaron el holandés Nijboer y el portugués Silva. Su ventaja llegó a ser de casi 10 minutos, pero la iniciativa del equipo del líder, el Alfa Lum, en la cabeza del pelotón, y la beligerancia de los favoritos en los últimos kilómetros hizo que Nijboer ganara con el primer grupo pisándole los talones.</p>
            <p class="text-justify">En el kilómetro 126 abandonó el holandés Mathieu Hermans (Seúr), con problemas estomacales.</p>
            <hr>
            <h4>Clasificaciones</h4>
            <p class="text-justify">1, Nijboer (Stuttgart) 5.14.50 horas. 2, Tebaldi (Chateau D'Ax), a un segundo. 3, Uwe Raab (PDM), a ocho segundos. 9, Miguel Indurain, m.t. 11, Pedro Delgado, m.t. 12, Fabio Parra, m.t. 13, Anselmo Fuerte, m.t. 14, Julián Gorospe, m.t. 48, Alvaro Pino, a un minuto. 51, Peio Ruiz Cabestany, m.t. 93, Marino Lejarreta, m.t. 98, Bernard, a 1.42.</p>
            <h4>General</h4>
            <p class="text-justify">1, Klimov, (Alfa Lum). 2, Marek Kulas (Diana), a 1.16 minutos. 3, Cuadrado (Mavisa), a 5.04. 4, Anselmo Fuerte (ONCE), a 7.05. 5, Johnny WeItz (ONCE), a 7.22. 6, Emonds (Teka), a 7.25. 7, Raab (PDM), a 7.36. 8, Gastón (Clas), a 7.46. 9, Blanco (Lotus), a 7.51. 10, Joaquín Hernández (Seur), a 7.56. 12, Cabestany (ONCE), a 7.5 7. 14, Indurain (Banesto), a 8.02. 15, Delgado (Banesto), a 8.02. 22, Lejarreta (ONCE), a 8.14. 40, Pino (Seur), a 8.49. 63, Bernard (Toshiba), a 9,27.</p>
        </div>
        <div class="col-2 card shadow-sm" style="background-color: buttonhighlight">
            <h3 style="text-align: center">Noticias</h3>
            <div class="row">
                <div class="col card shadow-sm">
                    <h4 class="text-justify">Los directores de equipos asumen el riesgo de sus tácticas</h4>
                    <p class="text-justify">Los directores deportivos de los equipos españoles asumen el notable riesgo del peligroso juego táctico que han utilizado en las seis primeras etapas de la Vuelta 
                       <?= Html::a('Seguir leyendo...',['site/noticia','num'=>'2'])?>
                    </p>
                </div>
            </div>
             <div class="row">
                <div class="col card shadow-sm">
                    <h4 class="text-justify">Gorospe cedio el liderato al italiano Giovannetti</h4>
                    <p class="text-justify">Un aspirante al mero triunfo de etapa, Carlos Hernández (Lotus), 
                       volvió a demostrar ayer que los favoritos 
                       <?= Html::a('Seguir leyendo...',['site/noticia','num'=>''])?>
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>