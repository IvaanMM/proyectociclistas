<?php
use yii\helpers\Html;
$this->title = 'Noticias';
?>


<div class="container">
    <div class="row stilobody">
        <div class="align-items-center col-10" style="padding: 30px">
            <p>VUELTA CICLISTA A ESPAÑA</p>
            <h1>Los directores de equipos asumen el riesgo de sus tácticas</h1>
            <hr>
            <p class="text-justify">Los directores deportivos de los equipos españoles asumen el notable riesgo del peligroso juego táctico que han utilizado en las seis primeras etapas de la Vuelta. El resultado es que los principales favoritos han cedido un tiempo importante respecto a corredores que, sin ser jefes de fila, pueden dar motivos a múltiples especulaciones sobre su opción para mantener sus actuales posiciones. José Miguel Echávarri (Banesto) explica su criterio sobre lo que ha sucedido hasta el momento Refiriéndose a Gorospe, Giovanetti, Cadena, Ivanov y Farfan, explica: "Hasta ahora esta mos poniendo sobre el tapete caballos y sotas y nos reservamos los ases y los reyes. Pero es un juego muy peligroso. Nadie puede estar tranquilo con esta situación, aunque nuestra posición es mejor que la de otros".Existe coincidencia unánime en definir como una "locura" el desarrollo de la carrera. No obstante, Juan Fernández (Clas) cree hasta cierto punto razonable que "todos hayamos preferido que por el momento los riesgos no los asuman nuestras mejores bazas para no machacar a nuestros equipos". Rafael Carrasco (Kelme), opina que la clasificación general "está como debe estar por la forma en la que ha ido la carrera, muy rápida y alocada. Pero estoy convencido que la lógica irá imponiéndose con el paso de los días". El máximo responsable del Kelme mantiene la teoría de que el equipo Banesto, ya antes incluso de que situara como líder a Gorospe "debía controlar porque es el más fuerte y el único que cuenta con un líder capaz de romper la carrera que es Delgado".</p>
            <p class="text-justify">Para González Linares (Teka) todos han actuado hasta el momento sin excesiva coherencia. "Si en la etapa del domingo, el Clas no llega a tirar del pelotón, los escapados hubieran sentenciado la Vuelta. Pero creo que va a ser difícil desbancar a corredores que se han situado muy bien como Ivanov, Giovanetti y Cadena". A pesar de la desventaja de los favoritos y de las especulaciones sobre la permanencia de Gorospe en el primer puesto, existe también unanimidad en cuanto a que los pronósticos iniciales siguen intactos. "No ha cambiado en absoluto la estrategia para el desenlace de la Vuelta, a pesar de que Gorospe haya roto el cerco de los grandes favoritos", aseguró Echávarri. 'To de Giovanetti y Unzaga", asegura Maximino Pérez (Seur) fue muy bonito y beneficioso para el equipo. Pero lo mejor que nos pasó el domingo fue que Pino no necesitó ser infiltrado porque está muy recuperado de su tendinitis".</p>
        </div>
        <div class="col-2 card shadow-sm" style="background-color: buttonhighlight">
            <h3 style="text-align: center">Noticias</h3>
            <div class="row">
                <div class="col card shadow-sm">
                    <h4 class="text-justify">Primer golpe de mano de Banesto en la Vuelta</h4>
                    <p class="text-justify">La omnipresencia parece ser la regla de (oro para todo aspirante a ganar la actual edición de la Vuelta a España. Desaparecer de la cabeza del pelotón, tan siquiera fugazmente, conduce directamente al furgón de cola
                       <?= Html::a('Seguir leyendo...',['site/noticia','num'=>'3'])?>
                    </p>
                </div>
            </div>
             <div class="row">
                <div class="col card shadow-sm">
                    <h4 class="text-justify">Gorospe cedio el liderato al italiano Giovannetti</h4>
                    <p class="text-justify">Un aspirante al mero triunfo de etapa, Carlos Hernández (Lotus), 
                       volvió a demostrar ayer que los favoritos 
                       <?= Html::a('Seguir leyendo...',['site/noticia','num'=>''])?>
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>