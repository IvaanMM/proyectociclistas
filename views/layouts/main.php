<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100 ">
<head>
    
<!--   MODIFICACION FAVICON-->

   <link rel="shortcut icon" href="/favicon2.ico" type="image/x-icon" />
    
<!--   MODIFICACION FAVICON-->   


    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
     
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body>
<?php $this->beginBody() ?>

    <header >
        <?php

        NavBar::begin([
            'brandLabel' => Html::img('@web/images/Logo.png',['style'=>'height:60px;'],['class'=>'navbar-brand'],['alt' => 'Logo']),
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-light navbar-expand-md navbar-dark bg-dark fixed-top justify-content-center shadow-sm',
                'style'=> 'height: 60px',
            ],
        ]);
        if(Yii::$app->user->isGuest){
                echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-light'],
            'items' => [
                ['label' => 'Noticias', 'url' => ['/site/noticias']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                ['label' => 'Login', 'url' => ['/site/login']]
            ],
        ]);
        }else{
                echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                ['label' => 'Noticias', 'url' => ['/site/noticias']],
                ['label' => 'Datos', 'url' => ['/site/datos']],
                ['label' => 'Contact', 'url' => ['/site/contact']],

                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'

            ],
        ]);
        }
        NavBar::end();
        ?>
    </header>

<main class="container-fluid">
    <?= Alert::widget() ?>
    <?= $content ?>
</main>

    <footer class="footer mt-auto py-3 text-muted">
        <div class="container-md">
            <p class="float-right" style="color: #ffffff">&copy; La vuelta <?= date('Y') ?></p>
        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
