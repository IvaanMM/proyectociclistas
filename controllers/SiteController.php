<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->actionTopciclistas();
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    public function actionNoticias() {
        return $this->render('noticias');
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
        
    public function actionDatos(){
        return $this->render('datos');
    }   
    
    public function actionNoticia($num){        
        return $this->render('noticia'.$num);
    }
    
    /*
     * esta funcion se ejecuta desde la funcion actionIndex, lo que hace es realizar una consulta y devuelve un render 
     * y tres variables donde se guardan los datos de las consultas para poder usarlos en la vista
     */
    
    public function actionTopciclistas(){
        //consulta que calcula el top 3 de equipos
        $topEquipos = Yii::$app->db->createCommand("select nomequipo from(SELECT e.dorsal, COUNT(*) AS victorias, c.nomequipo FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY c.nomequipo ORDER BY victorias DESC LIMIT 3) AS a")->queryAll();
        // consulta que calcula el top 3 de escaladores
        $topEscaladores = Yii::$app->db->createCommand("SELECT nombre FROM (SELECT p.dorsal, c.nombre, COUNT(*) AS victorias FROM  puerto p LEFT JOIN ciclista c ON p.dorsal = c.dorsal GROUP BY p.dorsal ORDER BY victorias DESC LIMIT 3) AS a")->queryAll();
        // consulta que calcula el top 3 de ciclistas
        $dorsalGanador = Yii::$app->db->createCommand("SELECT nombre FROM(SELECT e.dorsal, COUNT(*) AS victorias, c.nombre FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY dorsal ORDER BY victorias DESC limit 3) AS a")->queryAll();
        
        return $this->render("index",[
            "dorsal"=>$dorsalGanador,
            "topequipos"=>$topEquipos,
            "topescaladores"=>$topEscaladores,
        ]);
    }
    
}

// ------CONSULTAS-------

/*
 * consulta top 3 ciclista de la vuelta
 * 
 * SELECT nombre FROM(SELECT e.dorsal, COUNT(*) AS victorias, c.nombre FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY dorsal ORDER BY victorias DESC limit 3) AS a
 */

/*
 * consulta top 3 mejores equipos
 * 
 * select nomequipo from(SELECT e.dorsal, COUNT(*) AS victorias, c.nomequipo FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY c.nomequipo ORDER BY victorias DESC LIMIT 3) AS a
 */

/*
 * consulta usada para sacar los tres mejores escaladores de la vielta
 * SELECT nombre FROM (SELECT p.dorsal, c.nombre, COUNT(*) AS victorias FROM  puerto p LEFT JOIN ciclista c ON p.dorsal = c.dorsal GROUP BY p.dorsal ORDER BY victorias DESC LIMIT 3) AS a
 */